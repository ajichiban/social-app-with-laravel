<?php

use App\Comment;
use App\Friendship;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);


        factory(User::class)->create([
        	'email'=> 'test@gmail.com',
        	'password' => bcrypt(123456)
        ]);

        factory(User::class, 5)->create()
            ->each(function (User $user) {
                factory(Friendship::class)->create([
                    'sender_id' => $user->id,
                    'recipient_id' => 1
                ]);
            });

        factory(Comment::class, 10)->create();
    }
}
