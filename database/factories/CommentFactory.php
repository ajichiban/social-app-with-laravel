<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'body' => $faker->paragraph,
        'status_id' => function(){
        	return factory(App\Status::class)->create();
        },
        'user_id' => function(){
        	return factory(App\User::class)->create();
        }
    ];
});
