<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Friendship;
use App\User;
use Faker\Generator as Faker;

$factory->define(Friendship::class, function (Faker $faker) {
    return [
      'sender_id' => fn() => factory(User::class)->create(),
      'recipient_id' => fn() => factory(User::class)->create(),
    ];
});
