@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">

			<div class="col-md-3">
				<div class="card border-0 shadow-sm">
					<img src="{{ $user->avatar() }}" class="card-img-top">
					<div class="card-body text-center">
						<h5 class="card-title">{{ $user->name }} </h5>

						<button-friendship
							dusk="btn-request-friendship" 
							:recipient="{{ $user }}"
							friendship-status="{{ $friendshipStatus }}"
							>
						</button-friendship>

					</div><!-- ./card-body -->
				</div><!-- ./card -->
			</div><!-- ./col -->

			<div class="col-md-9">
				<status-list
					url="{{ route('users.statuses.index', $user) }}"
					>
				</status-list>
			</div>

		</div><!-- ./row -->
	</div><!-- ./container -->
@endsection

