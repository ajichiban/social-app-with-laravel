@extends('layouts.app')

@section('content')
	@forelse ($requestFriendships as $requestFriendship)

		<button-accept-friendship
			:sender="{{ $requestFriendship->sender }}"
			friendships-status="{{ $requestFriendship->status }}"
			>
			
		</button-accept-friendship>
	@empty
		<h4>no tienes ninguna solicitud pendiente</h4>
	@endforelse
@endsection