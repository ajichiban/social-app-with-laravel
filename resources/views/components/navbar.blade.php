<nav class="navbar navbar-expand-lg navbar-light navbar-laravel">
  <div class="container">
    <a class="navbar-brand" href="{{ route('home') }}">
      <i class="fa fa-address-book text-primary mr-1"></i>
      SocialApp
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">

      <ul class="navbar-nav mx-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        
        {{-- <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        </li> --}}
      </ul>

      <ul class="navbar-nav ml-auto">
        
        @guest
          <li class="nav-item"> 
            <a href="{{ route('register') }}">Register</a>
            <a href="{{ route('login') }}">Login</a>
          </li>
        @else
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ auth()->user()->email }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              @auth
                <a class="dropdown-item" href="{{ route('users.show', auth()->user()) }}">Perfil</a>
              @endauth
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a 
                onclick="document.querySelector('#form-logout').submit()" 
                class="dropdown-item" 
                href="#">Logout</a>
                <form id="form-logout" method="POST" action="{{ route('logout') }}">
                  @csrf
                </form>
            </div>
          </li>
        @endguest 
        
      </ul>
      {{-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> --}}
    </div>
  </div>
</nav>