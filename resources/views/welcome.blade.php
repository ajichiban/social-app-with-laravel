@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card border-0 shadow-sm">
                
                <status-form></status-form>

            </div>

            <!-- List Statuses -->
            <div class="mt-2">
            	<status-list img="{{ asset('img/sonreir.png') }}"></status-list>
            </div>
        </div>
    </div>
@endsection