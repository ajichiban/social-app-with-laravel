<?php

namespace App\Traits;

use App\Like;

trait HasLike 
{
	# Relacion polimorfica.
	public function likes()
  {
  	return $this->morphMany(Like::class, 'likeable');
  }

  public function doLike()
  {
  	$this->likes()->firstOrCreate([
  		'user_id' => auth()->id()
  	]);
  }

  public function doUnLike()
  {
      $this->likes()->whereUserId(auth()->id())->delete();
  }

  public function isLiked()
  {
      return $this->likes()->whereUserId(auth()->id())->exists();
  }

  public function likeCount()
  {
      return $this->likes()->count();
  }
}
