<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusesLikesController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
	}



    public function store(Status $status)
    {

    	$status->doLike();
    	/*$status->likes()->create([
    		'user_id' => auth()->id()
    	]);*/
    }

    public function destroy(Status $status)
    {
        $status->doUnLike();
    }
}
