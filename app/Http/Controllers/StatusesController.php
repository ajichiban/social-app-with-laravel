<?php

namespace App\Http\Controllers;

use App\Http\Resources\StatusResource;
use App\Status;
use Illuminate\Http\Request;

class StatusesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth', ['except' => ['index']]);
	}

    public function index()
    {

        return  StatusResource::collection(Status::latest()->paginate()); 
        
    }

    public function store()
    {
    	request()->validate([
    		'body' => 'required|min:5'
    	]);

    	$status = Status::create(array_merge(request()->all() , ['user_id' => auth()->id()]));

    	return StatusResource::make($status);
    }
}
