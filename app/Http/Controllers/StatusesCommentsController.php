<?php

namespace App\Http\Controllers;

use App\Http\Resources\CommentResource;
use App\{Status, Comment};
use Illuminate\Http\Request;

class StatusesCommentsController extends Controller
{
	

	public function __construct()
	{
		$this->middleware('auth');
	}



    public function store(Status $status)
    {
        request()->validate([
            'body' => 'required'
        ]);

    	$comment = Comment::create([
    		'user_id' => auth()->id(),
    		'status_id' => $status->id,
    		'body' => request('body')
    	]);

    	return CommentResource::make($comment);
    }
}
