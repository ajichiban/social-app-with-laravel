<?php

namespace App\Http\Controllers;

use App\Friendship;
use App\User;
use Illuminate\Http\Request;

class FriendshipsController extends Controller
{
  public function store(User $recipient)
  {
    $friendships = Friendship::FirstOrcreate([
    	'sender_id' => auth()->id(),
    	'recipient_id' => $recipient->id,
    ]);

    return response()->json(['friendship_status' => $friendships->fresh()->status]);
  }

  public function destroy(User $user)
  {
    $friendships = Friendship::where([
    	'sender_id' => auth()->id(),
    	'recipient_id' => $user->id,
    ])
    ->orWhere([
      'sender_id' => $user->id,
      'recipient_id' => auth()->id(),
    ])
    ->first();

    if ($friendships->status === 'denied') {
      return response()->json(['friendship_status' => 'denied']);
    }

    return response()->json(['friendship_status' => $friendships->delete() ? 'deleted' : '']);
  }
}
