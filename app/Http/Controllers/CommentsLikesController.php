<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentsLikesController extends Controller
{
    public function store(Comment $comment)
    {
    	$comment->likes()->create([
    		'user_id' => auth()->id()
    	]);
    }

    public function destroy(Comment $comment)
    {
    	$comment
    		->likes()
    		->whereUserId(auth()->id())
    		->delete();
    }
}
