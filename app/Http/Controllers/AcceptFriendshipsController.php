<?php

namespace App\Http\Controllers;

use App\Friendship;
use App\User;
use Illuminate\Http\Request;

class AcceptFriendshipsController extends Controller
{
  public function index()
  {
    $requestFriendships = Friendship::with('sender')->where([
      'recipient_id' => auth()->id()
    ])->get();

    return view('friendship.index', compact('requestFriendships'));
  }

  public function store(User $sender)
  {
    $updated = Friendship::where([
    	'sender_id' => $sender->id,
    	'recipient_id' => auth()->id(),
    	'status' => 'pending'
    ])->update([
    	'status' => 'accepted'
    ]);

    return response()->json([
      'friendship_status' => $updated  ? 'accepted' : ''
    ]);
  }

  public function destroy(User $sender)
  {
    $deleted  = Friendship::where([
    	'sender_id' => $sender->id,
    	'recipient_id' => auth()->id(),
    	'status' => 'pending'
    ])->update([
    	'status' => 'denied'
    ]);

    return response()->json([
      'friendship_status' => $deleted ? 'denied' : ''
    ]);
  }

}
