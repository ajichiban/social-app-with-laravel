<?php

namespace App\ModelExample;

use App\Traits\HasLike;
use Illuminate\Database\Eloquent\Model;

class ModelWithLikes extends Model
{
	use HasLike;
  protected $fillable = ['id'];
}
