<?php

namespace App;

use App\Traits\HasLike;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasLike;
    protected $fillable = ['body', 'user_id'];


    # Relaciones.

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    # Custom Methods.

}
