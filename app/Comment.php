<?php

namespace App;

use App\Traits\HasLike;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasLike;
    
    protected $fillable = ['user_id', 'status_id', 'body'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    # Custom Methods.

}
