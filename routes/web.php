<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
# Home Routes.
Route::get('/', 'HomeController@index')->name('home');

# Statuses Routes.
Route::post('statuses', 'StatusesController@store')->name('statuses.store');
Route::get('statuses', 'StatusesController@index')->name('statuses.index');

# Statuses Likes Routes.
Route::post('/statuses/{status}/likes', 'StatusesLikesController@store')
	->name('statuses.likes.store');

Route::delete('/statuses/{status}/likes','StatusesLikesController@destroy')
	->name('statuses.likes.destroy');

# Statuses Comments Routes.
Route::post('/statuses/{status}/comments', 'StatusesCommentsController@store')
	->name('statuses.comments.store');

# Comments Likes Routes.
Route::post('/comments/{comment}/likes', 'CommentsLikesController@store')
	->name('comments.likes.store')
	->middleware('auth');

Route::delete('/comments/{comment}/likes', 'CommentsLikesController@destroy')
	->name('comments.likes.destroy')
	->middleware('auth');

# User Routes.
Route::get('@{user}', 'UserController@show')->name('users.show');

# User status routes.
Route::get('users/{user}/statuses', 'UserStatusController@index')->name('users.statuses.index');

# Friendship route.
Route::post('friendships/{recipient}', 'FriendshipsController@store')
	->name('friendships.store')
	->middleware('auth');

Route::delete('friendships/{user}', 'FriendshipsController@destroy')
	->name('friendships.destroy')
	->middleware('auth');

# Friend Accept route.
Route::get('friends/requests', 'AcceptFriendshipsController@index')
	->name('accept-friendships.index')
	->middleware('auth');

Route::post('accept-friendships/{sender}', 'AcceptFriendshipsController@store')
	->name('accept-friendships.store')
	->middleware('auth');

Route::delete('accept-friendships/{sender}', 'AcceptFriendshipsController@destroy')
	->name('accept-friendships.destroy')
	->middleware('auth');

# Auth Routes.
Auth::routes();

