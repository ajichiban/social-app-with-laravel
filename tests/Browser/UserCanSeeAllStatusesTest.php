<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserCanSeeAllStatusesTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * @test void
     */
    public function users_can_see_all_statuses_on_homepage()
    {
        $statuses = factory(\App\Status::class, 3)->create();

        $this->browse(function (Browser $browser) use($statuses){

            $browser->visit('/')
                    ->waitForText($statuses->first()->body);
            
            foreach ($statuses as $status) {
                $browser->assertSee($status->body)
                        ->assertSee($status->user->name);
            }
        });
    }
}
