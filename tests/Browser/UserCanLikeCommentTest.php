<?php

namespace Tests\Browser;

use App\Comment;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserCanLikeCommentTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @test
     */
    public function user_can_like_and_unlike_comments_also_can_see_likes_count()
    {
        $user = factory(User::class)->create();
        $comment = factory(Comment::class)->create();

        $this->browse(function (Browser $browser) use ($user, $comment) {

            $browser->loginAs($user)
                ->visit('/')
                ->waitForText($comment->body)
                ->waitFor('@comment-likes-count', 30)
                ->assertSeeIn('@comment-likes-count', 0)

                # Do like
                ->press('@btn-like-comment')
                ->waitForText('TE GUSTA')
                ->assertSee('TE GUSTA')
                ->assertSeeIn('@comment-likes-count', 1)

                # Do unlike
                ->press('@btn-like-comment')
                ->waitForText('ME GUSTA')
                ->assertSee('ME GUSTA')
                ->assertSeeIn('@comment-likes-count', 0);

        });
    }
}
