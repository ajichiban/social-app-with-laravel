<?php

namespace Tests\Browser;

use App\Comment;
use App\{Status, User};
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserCanCommentStatusTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @test void
     */
    public function authenticated_users_can_comment_status()
    {
        $status = factory(Status::class)->create();
        $user = factory(User::class)->create();
        $comment = 'mi comentario';

        $this->browse(function (Browser $browser) use ($status, $user, $comment) {
            
            $browser->loginAs($user)
                    ->visit('/')
                    ->waitForText($status->body)
                    ->type('comment', $comment)
                    ->press('@btn-comment')
                    ->waitForText($comment)
                    ->assertSee($comment);
        });
    }

    /** @test*/
    public function users_can_see_all_comments()
    {
        $status = factory(Status::class)->create();
        $comments = factory(Comment::class, 2)->create(['status_id' => $status->id]);

        $this->browse(function (Browser $browser) use ($status, $comments) {
            
            $browser->visit('/')
                    ->waitForText($status->body);
                   /* ->assertSee($comments->shift()->body)
                    ->assertSee($comments->shift()->body);*/

            foreach ($comments as $comment) {
                 $browser->assertSee($comment->body)
                         ->assertSee($comment->user->name);
            }

        });

    }
}
