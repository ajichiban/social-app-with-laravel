<?php

namespace Tests\Browser;

use App\Friendship;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UsersCanRequestFriendshipTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @test void
     */
    public function senders_can_create_and_delete_request_friendship()
    {
        $sender = factory(User::class)->create();
        $recipient = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($sender)
                ->visit(route('users.show', $recipient))
                ->press('@btn-request-friendship')
                ->waitForText('cancelar solicitud')
                ->assertSee('cancelar solicitud')
                ->visit(route('users.show', $recipient))
                ->assertSee('cancelar solicitud')
                ->press('@btn-request-friendship')
                ->waitForText('solicitar amistad')
                ->assertSee('solicitar amistad');

        });
    }

    /**
     * @test void
     */
    public function senders_can_delete_accepted_request_friendship()
    {
        $sender = factory(User::class)->create();
        $recipient = factory(User::class)->create();

        Friendship::create([
            'sender_id' => $sender->id,
            'recipient_id' => $recipient->id,
            'status' => 'accepted'
        ]);

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($sender)
                ->visit(route('users.show', $recipient))
                ->assertSee('Eliminar de mis amigos')
                ->press('@btn-request-friendship')
                ->waitForText('solicitar amistad')
                ->assertSee('solicitar amistad')
                ->visit(route('users.show', $recipient))
                ->waitForText('solicitar amistad')
                ->assertSee('solicitar amistad');

        });
    }

    /**
     * @test void
     */
    public function senders_cannot_delete_denied_request_friendship()
    {
        $sender = factory(User::class)->create();
        $recipient = factory(User::class)->create();

        Friendship::create([
            'sender_id' => $sender->id,
            'recipient_id' => $recipient->id,
            'status' => 'denied'
        ]);

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($sender)
                ->visit(route('users.show', $recipient))
                ->assertSee('solicitud denegada')
                ->press('@btn-request-friendship')
                ->waitForText('solicitud denegada')
                ->assertSee('solicitud denegada');
        });
    }

    /**
    *@test
    */
    public function recipient_can_accept_friendship()
    {
        $sender = factory(User::class)->create(['name' => 'augusto']);
        $recipient = factory(User::class)->create();

        Friendship::create([
            'sender_id' => $sender->id,
            'recipient_id' => $recipient->id,
        ]);

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($recipient)
                ->visit(route('accept-friendships.index'))
                ->assertSee($sender->name)
                ->press('@btn-accept-friendship')
                ->waitForText('son amigos')
                ->assertSee('son amigos')
                ->visit(route('accept-friendships.index'))
                ->assertSee('son amigos');

        });
    }

    /**
    *@test
    */
    public function recipient_can_deny_friendship()
    {
        $sender = factory(User::class)->create(['name' => 'augusto']);
        $recipient = factory(User::class)->create();

        Friendship::create([
            'sender_id' => $sender->id,
            'recipient_id' => $recipient->id,
        ]);

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($recipient)
                ->visit(route('accept-friendships.index'))
                ->assertSee($sender->name)
                ->press('@btn-deny-friendship')
                ->waitForText('solicitud denegada')
                ->assertSee('solicitud denegada')
                ->visit(route('accept-friendships.index'))
                ->assertSee('solicitud denegada');

        });
    }

    /**
    *@test
    */
    public function recipient_can_delete_friendship()
    {
        $sender = factory(User::class)->create(['name' => 'augusto']);
        $recipient = factory(User::class)->create();

        Friendship::create([
            'sender_id' => $sender->id,
            'recipient_id' => $recipient->id,
        ]);

        $this->browse(function (Browser $browser) use ($sender, $recipient){
            $browser->loginAS($recipient)
                ->visit(route('accept-friendships.index'))
                ->assertSee($sender->name)
                ->press('@btn-delete-friendship')
                ->waitForText('solicitud eliminada')
                ->assertSee('solicitud eliminada')
                ->visit(route('accept-friendships.index'))
                ->assertDontSee('solicitud eliminada');

        });
    }
}
