<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

use App\User;

class UserCanCreateStatusesTest extends DuskTestCase
{
    use DatabaseMigrations;
    /** @test*/
    public function users_can_create_statuses()
    {
        $user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use($user) {

            $browser->loginAs($user)
                    ->visit('/')
                    ->type('body', 'mi primer status')
                    ->press('#create-status')
                    ->waitForText('mi primer status')
                    ->assertSee('mi primer status')
                    ->assertSee($user->name);

        });
    }
}
