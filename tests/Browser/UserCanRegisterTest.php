<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UserCanRegisterTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * A Dusk test example.
     *
     * @test void
     */
    public function user_can_register()
    {
        $this->withoutExceptionHandling();

        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                ->type('name', 'ajichiban')
                ->type('first_name', 'augusto')
                ->type('last_name', 'Rengifo')
                ->type('email', 'ajichiban@gmail.com')
                ->type('password', '12345678')
                ->type('password_confirmation', '12345678')
                ->press('@btn-register')
                ->assertPathIs('/')
                ->assertAuthenticated();
        });
    }

    /** 
     * @test
     */
    public function user_cannot_register_with_invalid_information()
    {
        $this->withoutExceptionHandling();
        
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                ->type('name', '')
                ->press('@btn-register')
                ->assertPathIs('/register')
                ->assertPresent('@validation-errors');
        });
    }
}
