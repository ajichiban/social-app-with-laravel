<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\User;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;
    /**
     * @test
     *
     * @return void
     */
    public function registered_users_can_login()
    {
        factory(User::class)->create(['email' => 'ajichiban@gmail.com']);

        $this->browse(function (Browser $browser) {

            $browser->visit('/login')
                    ->type('email', 'ajichiban@gmail.com')
                    ->type('password','password')
                    ->press('@btn-login')
                    ->assertPathIs('/')
                    ->assertAuthenticated();
        });
    }

    /** 
     * @test
     */
    public function user_cannot_loging_with_invalid_information()
    {
        $this->withoutExceptionHandling();
        
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('email', '')
                ->press('@btn-login')
                ->assertPathIs('/login')
                ->assertPresent('@validation-errors');
        });
    }
}
