<?php

namespace Tests\Browser;

use App\Status;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class UsersCanLikesStatusesTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** 
    *@test
    */
    public function guest_can_not_like_statuses()
    {
        $status = factory(Status::class)->create();

        $this->browse(function (Browser $browser) use ($status) {

            $browser->visit('/')
                ->waitForText($status->body)
                # Do like
                ->press('@btn-like')
                ->waitFor('#btn-login')
                ->assertPathIs('/login');

        });
    }

    /**
     * A Dusk test example.
     *
     * @test
     */
    public function user_can_like_and_unlike_statuses_also_can_see_likes_count()
    {
        $user = factory(User::class)->create();
        $status = factory(Status::class)->create();

        $this->browse(function (Browser $browser) use ($user, $status) {

            $browser->loginAs($user)
                ->visit('/')
                ->waitForText($status->body)
                ->waitFor('@likes-count', 30)
                ->assertSeeIn('@likes-count', 0)

                # Do like
                ->press('@btn-like')
                ->waitForText('TE GUSTA')
                ->assertSee('TE GUSTA')
                ->assertSeeIn('@likes-count', 1)

                # Do unlike
                ->press('@btn-like')
                ->waitForText('ME GUSTA')
                ->assertSee('ME GUSTA')
                ->assertSeeIn('@likes-count', 0);

        });
    }

}
