<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CanSeeProfileTest extends TestCase
{
    use RefreshDatabase;
    /**
     * 
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     * @test
     */
    public function can_see_profile()
    {
        $this->withoutExceptionHandling();

        factory(User::class)->create(['name' => 'Augusto']);

        $this->get('@Augusto')
            ->assertSee('Augusto');
    }
}
