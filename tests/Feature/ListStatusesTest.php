<?php

namespace Tests\Feature;

use App\Status;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListStatusesTest extends TestCase
{
    use RefreshDatabase;
    /**
     * @test
     *
     * @return void
     */

    # Ademas que probar si se obtiene todos los datos , prueba si los devuelve en orden descendente.
    public function can_get_all_statuses()
    {
        $this->withoutExceptionHandling();

        $status1 = factory(\App\Status::class)->create(['created_at' => now()->subDays(4)]);
        $status2 = factory(\App\Status::class)->create(['created_at' => now()->subDays(3)]);
        $status3 = factory(\App\Status::class)->create(['created_at' => now()->subDays(2)]);
        $status4 = factory(\App\Status::class)->create(['created_at' => now()->subDays(1)]);

        $response = $this->getJson('statuses');

        $response->assertSuccessful();

        $response->assertJson([
            'meta' => ['total' => 4]
        ]);

        $response->assertJsonStructure([
            'data',
            'links'=>['prev', 'next']
        ]);

        $this->assertEquals(
            $status4->body,
            $response->json('data.0.body')
        );

        /*dd($response->json('data.0'));*/

    }

    /** 
     * @test
     */
    public function can_get_statuses_for_a_specific_user()
    {
        $this->withoutExceptionHandling();
        
        $user = factory(User::class)->create();

        $status1 = factory(Status::class)
            ->create([
                'user_id' => $user->id,
                'created_at' => now()->subDays()
            ]);

        $status2 = factory(Status::class)->create(['user_id' => $user->id]);

        $otherStatuses = factory(Status::class, 2)->create();

        $response = $this->actingAs($user)
            ->getJson(route('users.statuses.index',$user));


        $response->assertJson([
            'meta' => ['total' => 2]
        ]);

        $response->assertJsonStructure([
            'data', 'links' => ['prev', 'next']
        ]);


        $this->assertEquals(
            $status2->body,
            $response->json('data.0.body')
        );




    }
}
