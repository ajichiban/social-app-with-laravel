<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
  use RefreshDatabase;
  /**
   * 
   * Desactiva el manejo de excepciones.
   * $this->withoutExceptionHandling();
   *
   * @test
   */
  public function user_can_register()
  {
    $this->withoutExceptionHandling();
    
    $this->get(route('register'))->assertSuccessful();

    $response = $this->post(route('register'), $this->userValidData());

    $response->assertRedirect('/');

    $this->assertDataBaseHas('users', [
      'name' => 'AugustoR',
      'first_name' => 'Augusto',
      'last_name' => 'Rengifo',
      'email' => 'ajichiban@gmail.com',  
    ]);

    #Confirmar si el password esta incriptado.
    $this->assertTrue(
      Hash::check('12345678', User::first()->password),
      'the Password must be encrypted.'
    );
  }


  # FIELD NAME
  /** 
   * @test
   */
  public function the_name_is_required()
  {
    $this->post(
          route('register'),
          $this->userValidData(['name'=> null])
        )
        ->assertSessionHasErrors('name');
  }

  /** 
   * @test
   */
  public function the_name_must_be_string()
  {
    $this->post(
          route('register'),
          $this->userValidData(['name'=> 12345])
        )
        ->assertSessionHasErrors('name');
  }

  /** 
   * @test
   */
  public function the_name_may_not_be_greater_than_60_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['name'=>  Str::random(61) ])
        )
        ->assertSessionHasErrors('name');
  }

  /** 
   * @test
   */
  public function the_name_must_be_at_least_5_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['name'=> '1234'])
        )
        ->assertSessionHasErrors('name');
  }

  /** 
   * @test
   */
  public function the_name_must_be_unique()
  {
    factory(User::class)->create(['name' => 'augusto']);

    $this->post(
          route('register'),
          $this->userValidData(['name'=> 'augusto' ])
        )
        ->assertSessionHasErrors('name');
  }

  /** 
   * @test
   */
  public function the_name_must_only_contains_letter_and_number()
  {
    $this->post(
          route('register'),
          $this->userValidData(['name'=> 'augusto<>' ])
        )
        ->assertSessionHasErrors('name');
  }

  # FIELD FIRST_NAME

  /** 
   * @test
   */
  public function the_first_name_is_required()
  {
    $this->post(
          route('register'),
          $this->userValidData(['first_name'=> null])
        )
        ->assertSessionHasErrors('first_name');
  }

  /** 
   * @test
   */
  public function the_first_name_must_be_string()
  {
    $this->post(
          route('register'),
          $this->userValidData(['first_name'=> 12345])
        )
        ->assertSessionHasErrors('first_name');
  }

  /** 
   * @test
   */
  public function the_first_name_may_not_be_greater_than_60_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['first_name'=>  Str::random(61) ])
        )
        ->assertSessionHasErrors('first_name');
  }

  /** 
   * @test
   */
  public function the_first_name_must_be_at_least_5_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['first_name'=> '1234'])
        )
        ->assertSessionHasErrors('first_name');
  }

  /** 
   * @test
   */
  public function the_first_name_must_only_contains_letter()
  {
    $this->post(
          route('register'),
          $this->userValidData(['first_name'=> 'augusto55' ])
        )
        ->assertSessionHasErrors('first_name');
  }

  # FIELD LAST_NAME

  /** 
   * @test
   */
  public function the_last_name_is_required()
  {
    $this->post(
          route('register'),
          $this->userValidData(['last_name'=> null])
        )
        ->assertSessionHasErrors('last_name');
  }

  /** 
   * @test
   */
  public function the_last_name_must_be_string()
  {
    $this->post(
          route('register'),
          $this->userValidData(['last_name'=> 12345])
        )
        ->assertSessionHasErrors('last_name');
  }

  /** 
   * @test
   */
  public function the_last_name_may_not_be_greater_than_60_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['last_name'=>  Str::random(61) ])
        )
        ->assertSessionHasErrors('last_name');
  }

  /** 
   * @test
   */
  public function the_last_name_must_be_at_least_5_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['last_name'=> '1234'])
        )
        ->assertSessionHasErrors('last_name');
  }

  /** 
   * @test
   */
  public function the_last_name_must_only_contains_letter()
  {
    $this->post(
          route('register'),
          $this->userValidData(['last_name'=> 'augusto55' ])
        )
        ->assertSessionHasErrors('last_name');
  }

  # FIELD EMAIL

  /** 
   * @test
   */
  public function the_email_is_required()
  {
    $this->post(
          route('register'),
          $this->userValidData(['email'=> null])
        )
        ->assertSessionHasErrors('email');
  }

  /** 
   * @test
   */
  public function the_email_must_be_valid_email()
  {
    $this->post(
          route('register'),
          $this->userValidData(['email'=> 'augustorgmail'])
        )
        ->assertSessionHasErrors('email');
  }

  /** 
   * @test
   */
  public function the_email_may_not_be_greater_than_100_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['email'=>  Str::random(101) ])
        )
        ->assertSessionHasErrors('email');
  }

  /** 
   * @test
   */
  public function the_email_must_be_unique()
  {
    factory(User::class)->create(['email' => 'ajichiban@gmail.com']);

    $this->post(
          route('register'),
          $this->userValidData(['email'=> 'ajichiban@gmail.com' ])
        )
        ->assertSessionHasErrors('email');
  }

  # FIELD PASSWORD.

    /** 
   * @test
   */
  public function the_password_is_required()
  {
    $this->post(
          route('register'),
          $this->userValidData(['password'=> null])
        )
        ->assertSessionHasErrors('password');
  }

  /** 
   * @test
   */
  public function the_password_must_be_at_least_8_characters()
  {
    $this->post(
          route('register'),
          $this->userValidData(['password'=> '12345'])
        )
        ->assertSessionHasErrors('password');
  }

  /** 
   * @test
   */
  public function the_password_must_be_confirmated()
  {
    $this->post(
          route('register'),
          $this->userValidData([
            'password'=> '12345678',
            'password_confirmation' => null
          ])
        )
        ->assertSessionHasErrors('password');
  }


  # CUSTOM METHODS.
  public function userValidData($overrides = [])
  {
      return array_merge(
        [
          'name' => 'AugustoR',
          'first_name' => 'Augusto',
          'last_name' => 'Rengifo',
          'email' => 'ajichiban@gmail.com',
          'password' => '12345678',
          'password_confirmation' => '12345678'

        ], $overrides);
  }
}
