<?php

namespace Tests\Feature;

use App\Status;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateCommentsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Todos los codigos http : symfony/http-foundation/response.
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     * @test
     */
    public function authenticated_user_can_create_comments()
    {
        $this->withoutExceptionHandling();

        $status = factory(Status::class)->create();
        $user = factory(User::class)->create();
        $comment = ['body' => 'Mi primer comentario'];

        $response = $this->actingAs($user)->postJSon(route('statuses.comments.store', $status), $comment );

        $response->assertJSon([
            'data' => ['body' => $comment['body']]
        ]);

        $this->assertDatabaseHas('comments', [
            'user_id' => $user->id,
            'status_id' => $status->id,
            'body' => $comment['body']
        ]);
    }

    /** @test*/
    public function guest_can_not_create_comments()
    {
        /*$this->withoutExceptionHandling();*/

        $status = factory(Status::class)->create();
        $comment = ['body' => 'Mi primer comentario'];

        $response = $this->postJSon(route('statuses.comments.store', $status), $comment );

        $response->assertStatus(401);

    }

    /** @test*/
    public function a_comment_requires_a_body()
    {
       /* $this->withoutExceptionHandling();*/

        $status = factory(Status::class)->create();
        $user = factory(User::class)->create();
        
        $this->actingAs($user);

        $response = $this->postJson(route('statuses.comments.store',$status ),['body' => '']);

        # Entidad no procesable.
        $response->assertStatus(422);

        # Verifica la estructura de Json.
        $response->assertJSonStructure([
            'message',
            'errors' => ['body']
        ]);
        /*$response->assertSessionHasErrors('body');*/
    }
}
