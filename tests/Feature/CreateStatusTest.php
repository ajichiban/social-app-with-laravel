<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;



use App\User;

class CreateStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test*/
    public function guest_user_can_not_create_statuses()
    {
        /*$this->withoutExceptionHandling();*/

        $response = $this->postJson(route('statuses.store', ['body' => 'Mi primer estado']));

        /*dd($response->content());*/

        $response->assertStatus(401);
    }

    /** @test*/  /* <== para que Phpunit lo reconozca*/
    public function an_authenticated_user_can_create_statuses()
    {
        #Evitar que laravel maneje las excepciones.
        $this->withoutExceptionHandling();

        # Pasos para hacer un test.

        # 1. Given => teniendo , ej : Teniendo un usuario authenticado.
        $user = factory(User::class)->create();
        $this->actingAs($user);

        # 2. When => Cuando , ej: cuando hace un post request a status.
        $response = $this->post(route('statuses.store', ['body' => 'Mi primer estado']));

        $response->assertJSon([
            'data' => ['body' => 'Mi primer estado']
        ]);

        # 3. Then => Entonces , ej : veo n nuevo estado en la base de datos.
        $this->assertDatabaseHas('statuses', [
            'user_id' => $user->id,
            'body' => 'Mi primer estado'
        ]);
    }

    /** @test*/
    public function a_status_requires_a_body()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->postJson(route('statuses.store', ['body' => '']));

        # Entidad no procesable.
        $response->assertStatus(422);

        # Verifica la estructura de Json.
        $response->assertJSonStructure([
            'message',
            'errors' => ['body']
        ]);
        /*$response->assertSessionHasErrors('body');*/
    }

    /** @test*/
    public function a_status_body_requires_a_minimum_length()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->postJson(route('statuses.store', ['body' => 'abcd']));

        # Entidad no procesable.
        $response->assertStatus(422);

        # Verifica la estructura de Json.
        $response->assertJSonStructure([
            'message',
            'errors' => ['body']
        ]);
        /*$response->assertSessionHasErrors('body');*/
    }


}
