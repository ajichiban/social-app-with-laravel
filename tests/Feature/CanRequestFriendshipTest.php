<?php

namespace Tests\Feature;

use App\Friendship;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CanRequestFriendshipTest extends TestCase
{
  use RefreshDatabase;
  /**
   * 
   * Desactiva el manejo de excepciones.
   * $this->withoutExceptionHandling();
   *
   * @test
   */
  public function can_create_friendship_request_()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    $response = $this->actingAs($sender)->postJson(route('friendships.store', $recipient));

    $response->assertJson([
      'friendship_status'=> 'pending'
    ]);

    $this->assertDatabaseHas('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'pending'
    ]);

    $this->actingAs($sender)->postJson(route('friendships.store', $recipient));
    $this->assertCount(1, Friendship::all());
  }

  /**
  * @test
  */
  public function user_can_delete_sent_friendship_request_()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    Friendship::create([
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
    ]);

    $this->actingAs($sender)->deleteJson(route('friendships.destroy', $recipient));

    $this->assertDatabaseMissing('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
    ]);
  }

  /**
  * @test
  */
  public function sender_cannot_delete_denied_friendship_request_()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    Friendship::create([
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'denied'
    ]);

    $response = $this->actingAs($sender)->deleteJson(route('friendships.destroy', $recipient));

    $response->assertJson([
      'friendship_status' => 'denied'
    ]);



    $this->assertDatabaseHas('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'denied'
    ]);
  }

  /**
  * @test
  */
  public function users_can_acepted_friendship_request()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    Friendship::create([
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'pending'
    ]);

    $response = $this->actingAs($recipient)->postJson(route('accept-friendships.store', $sender));

    $response->assertJson([
      'friendship_status' => 'accepted'
    ]);

    $this->assertDatabaseHas('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'accepted'
    ]);
  }

  /**
  * @test
  */
  public function users_denied_friendship_request()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    Friendship::create([
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'pending'
    ]);

    $response = $this->actingAs($recipient)->deleteJson(route('accept-friendships.destroy', $sender));

    $response->assertJson([
      'friendship_status'=> 'denied'
    ]);

    $this->assertDatabaseHas('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
      'status' => 'denied'
    ]);
  }

  /**
  * @test
  */
  public function  guest_cannot_create_friendship_request_()
  {
    $recipient = factory(User::class)->create();

    $response = $this->postJson(route('friendships.store', $recipient));

    $response->assertStatus(401);

  }

  /**
  * @test
  */
  public function guest_cannot_delete_friendship_request_()
  {
    $recipient = factory(User::class)->create();

    $response = $this->deleteJson(route('friendships.destroy', $recipient));

    $response->assertStatus(401);
  }

  /**
  * @test
  */
  public function gues_cannot_acepted_friendship_request()
  {
    $sender = factory(User::class)->create();

    $response = $this->postJson(route('accept-friendships.store', $sender));

    $response->assertStatus(401);
  }

  /**
  * @test
  */
  public function gues_cannot_denied_friendship_request()
  {
    $sender = factory(User::class)->create();

    $response = $this->deleteJson(route('accept-friendships.destroy', $sender));

    $response->assertStatus(401);
  }

  /**
  * @test
  */
  public function user_can_delete_received_friendship_request_()
  {
    $this->withoutExceptionHandling();

    $sender = factory(User::class)->create();
    $recipient = factory(User::class)->create();

    Friendship::create([
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
    ]);

    $this->actingAs($recipient)->deleteJson(route('friendships.destroy', $sender));

    $this->assertDatabaseMissing('friendships', [
      'sender_id' => $sender->id,
      'recipient_id' => $recipient->id,
    ]);
  }

}
