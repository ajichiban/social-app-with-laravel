<?php

namespace Tests\Unit\Traits;

use App\Like;
use App\ModelExample\ModelWithLikes;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class HasLikeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     *@test
     */
    public function a_model_morph_many_likes()
    {
        $model = new ModelWithLikes(['id' => 1]);

       /* $comment = factory(Comment::class)->create();*/

        factory(Like::class)->create([
            'likeable_id' => $model->id,
            'likeable_type' => get_class($model)
        ]);

        $this->assertInstanceOf(Like::class, $model->likes->first());
    }

    /** @test*/
    public function a_model_can_be_liked_and_unliked()
    {
        $model = new ModelWithLikes(['id' => 1]);

        $this->actingAs(factory(User::class)->create());

        $model->doLike();

        $this->assertEquals(1, $model->likes()->count());

        $model->doUnLike();

        $this->assertEquals(0, $model->likes()->count());
    }

    /** 
    * @test
    */
    public function a_model_can_be_liked_once()
    {
        $model = new ModelWithLikes(['id' => 1]);

        $this->actingAs(factory(User::class)->create());

        $model->doLike();

        $this->assertEquals(1, $model->likes()->count());

        $model->doLike();

        $this->assertEquals(1, $model->likes()->count());
    }

    /** 
    *@test
    */
    public function a_model_know_if_it_has_been_liked()
    {
        $model = new ModelWithLikes(['id' => 1]);

        $this->assertFalse($model->isLiked());

        $this->actingAs(factory(User::class)->create());

        $model->doLike();

        $this->assertTrue($model->isLiked());
    }

    /** 
    *@test
    */
    public function a_model_knows_how_many_likes_it_has()
    {
        $model = new ModelWithLikes(['id' => 1]);
           /* ->each(function(model $c){
                $c->likes()->saveMany(
                    factory(Like::class, 2)->make()
                );
            });*/
        $model->likes()->saveMany(
            factory(Like::class,2)->make()
        );
        /*factory(Like::class, 2)->create([
            'likeable_id' => $model->id,
            'likeable_type' => get_class($model)
        ]);*/

        /*factory(Like::class, 2)->create(['model_id' => $model->id]);*/

        $this->assertEquals(2, $model->likeCount());
    }
}
