<?php

namespace Tests\Unit\Models;

use App\Traits\HasLike;
use App\{Status,User, Like, Comment};
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class StatusTest extends TestCase
{
	use RefreshDatabase;
    /**
   	 *@test
     */
    public function a_status_belongs_to_a_user()
    {
        $status = factory(Status::class)->create();

        $this->assertInstanceOf(User::class, $status->user);
    }

    /** @test*/
    public function a_status_has_many_comments()
    {
        $status = factory(Status::class)->create();

        factory(Comment::class)->create(['status_id' => $status->id]);

        $this->assertInstanceOf(Comment::class, $status->comments->first());
    }

    /**
     *@test
     */
    public function a_status_model_must_use_the_trait_has_likes()
    {
        $this->assertClassUsesTrait(HasLike::class, Status::class);
    }

}
