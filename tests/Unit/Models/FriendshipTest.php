<?php

namespace Tests\Unit\Models;

use App\Friendship;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class FriendshipTest extends TestCase
{
  use RefreshDatabase;
  /**
   * Desactiva el manejo de excepciones.
   * $this->withoutExceptionHandling();
   *
   * @test
   */
  public function a_friendship_request_belongd_to_a_sender()
  {

    $sender = factory(User::class)->create();

    $friendship = factory(Friendship::class)->create(['sender_id' => $sender->id]);

    $this->assertInstanceOf(User::class, $friendship->sender);
  }

  /**
  * @test
  */
  public function a_friendship_request_belongd_to_a_recipient()
  {
    
    $recipient = factory(User::class)->create();

    $friendship = factory(Friendship::class)->create(['recipient_id' => $recipient->id]);

    $this->assertInstanceOf(User::class, $friendship->recipient);
  }
}
