<?php

namespace Tests\Unit\Models;

use App\Status;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     * @test
     */
    public function route_key_name_is_set_to_name()
    {
        $user = factory(User::class)->make();

        $this->assertEquals('name', $user->getRouteKeyName(), 'The route key name must be name');
    }

    /** 
     * @test
     */
    public function user_has_a_link_to_their_profile()
    {
        $user = factory(User::class)->make();

        $this->assertEquals(route('users.show', $user), $user->link());
    }

    /** 
     * @test
     */
    public function user_has_a_avatar()
    {
        $user = factory(User::class)->make();

        $this->assertEquals(asset('img/sonreir.png'), $user->avatar());

    }

     /** 
     * @test
     */
    public function a_user_has_many_statuses()
    {
        $user = factory(User::class)->create();

        factory(Status::class)->create(['user_id' => $user->id]);

        $this->assertInstanceOf(Status::class, $user->statuses->first());
    }
}
