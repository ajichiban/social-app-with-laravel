<?php

namespace Tests\Unit\Models;

use App\Comment;
use App\Like;
use App\Traits\HasLike;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use  RefreshDatabase;
    /**
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     *@test
     */
    public function a_comment_belongs_user()
    {
        $comment = factory(Comment::class)->create();

        $this->assertInstanceOf(User::class, $comment->user);
    }

    /**
     *@test
     */
    public function a_comment_model_must_use_the_trait_has_likes()
    {
        $this->assertClassUsesTrait(HasLike::class, Comment::class);
    }

}
