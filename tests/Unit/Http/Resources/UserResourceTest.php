<?php

namespace Tests\Unit\Http\Resources;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class UserResourceTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     * @test
     */
    public function a_user_resources_must_have_the_necesary_fields()
    {
        $this->withoutExceptionHandling();

        $user = factory(user::class)->create();

        $userResource = UserResource::make($user)->resolve();

        $this->assertEquals(
            $user->name,
            $userResource['name']
        );

        $this->assertEquals(
            $user->link(),
            $userResource['link']
        );

        $this->assertEquals(
            $user->avatar(),
            $userResource['avatar']
        );

       
    }
}
