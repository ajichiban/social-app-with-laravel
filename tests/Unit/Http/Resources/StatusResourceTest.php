<?php

namespace Tests\Unit\Http\Resources;

use App\Comment;
use App\Http\Resources\CommentResource;
use App\Http\Resources\StatusResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StatusResourceTest extends TestCase
{
	use RefreshDatabase;
    /**
     * @test
     *
     * @return void
     */
    public function a_status_resources_must_have_the_necesary_fields()
    {
    	

        $status = factory(\App\Status::class)->create();

        factory(Comment::class, 2)->create(['status_id' => $status->id]);

        # Se usa el metodo resolve() para simular la respuesta del recurso,
        # sin embargo aqui no es necesario , se coloca por fines ilustrativos.
        $statusResource = StatusResource::make($status)->resolve();

        # Compruba que dos valores sean iguales.
        $this->assertEquals(
            $status->id,
            $statusResource['id']
        );

        $this->assertEquals(
            $status->body,
            $statusResource['body']
        );

        $this->assertEquals(
            $status->user->name,
            $statusResource['user_name']
        );

        $this->assertEquals(
            $status->user->avatar(),
            $statusResource['user_avatar']
        );

        $this->assertEquals(
            $status->created_at->diffForHumans(),
            $statusResource['ago']
        );

        $this->assertEquals(
            $status->user->link(),
            $statusResource['user_link']
        );

        $this->assertEquals(
            false,
            $statusResource['is_liked']
        );

        $this->assertEquals(
            0,
            $statusResource['likes_count']
        );

        # Comprueba que el dato se un recurso (Collection)
        $this->assertEquals(
            CommentResource::class,
            $statusResource['comments']->collects
        );

        /*dd($statusResource['comments']->first()->resource);*/
        # Comprueba que la collction sea del modelo Comment
        $this->assertInstanceOf(
            Comment::class,
            $statusResource['comments']->first()->resource
        );


        $this->assertInstanceOf(
            UserResource::class,
            $statusResource['user']
        );

        $this->assertInstanceOf(
            User::class,
            $statusResource['user']->resource
        );



        # Comprueba que en un array se encuentre un determinada key.
        # $this->assertArrayHasKey('body', $statusResource);
        

    }
}
