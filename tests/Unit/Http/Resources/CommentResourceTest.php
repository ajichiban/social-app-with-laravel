<?php

namespace Tests\Unit\Http\Resources;

use App\Comment;
use App\Http\Resources\CommentResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use PHPUnit\Framework\TestCase as TestCase2;
use Tests\TestCase;

class CommentResourceTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Desactiva el manejo de excepciones.
     * $this->withoutExceptionHandling();
     *
     * @test
     */
    public function a_comment_resources_must_have_the_necesary_fields()
    {
        $this->withoutExceptionHandling();

        $comment = factory(Comment::class)->create();

        $commentResource = CommentResource::make($comment)->resolve();

       /* dd($commentResource);*/

        $this->assertEquals(
            $comment->id,
            $commentResource['id']
        );

        $this->assertEquals(
            $comment->body,
            $commentResource['body']
        );

        $this->assertEquals(
            $comment->user->name,
            $commentResource['user_name']
        );

        $this->assertEquals(
            $comment->user->link(),
            $commentResource['user_link']
        );

        $this->assertEquals(
            $comment->user->avatar(),
            $commentResource['user_avatar']
        );

        $this->assertEquals(
            false,
            $commentResource['is_liked']
        );

        $this->assertEquals(
            0,
            $commentResource['likes_count']
        );

        $this->assertInstanceOf(
            UserResource::class,
            $commentResource['user']
        );

        $this->assertInstanceOf(
            User::class,
            $commentResource['user']->resource
        );
    }
}
